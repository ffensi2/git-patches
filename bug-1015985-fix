# HG changeset patch
# Parent e86a0d92d174599678417ac1b11eff617f20ab40
# User Eric Phan <eric.phan@ensimag.grenoble-inp.fr>
CanSeek method in readers

diff --git a/content/html/content/src/HTMLMediaElement.cpp b/content/html/content/src/HTMLMediaElement.cpp
--- a/content/html/content/src/HTMLMediaElement.cpp
+++ b/content/html/content/src/HTMLMediaElement.cpp
@@ -40,16 +40,17 @@
 #include "jsapi.h"
 
 #include "nsITimer.h"
 
 #include "MediaError.h"
 #include "MediaDecoder.h"
 #include "nsICategoryManager.h"
 #include "MediaResource.h"
+#include "MediaDecoderStateMachine.h"
 
 #include "nsIDOMHTMLVideoElement.h"
 #include "nsIContentPolicy.h"
 #include "nsContentPolicyUtils.h"
 #include "nsCrossSiteListenerProxy.h"
 #include "nsCycleCollectionParticipant.h"
 #include "nsICachingChannel.h"
 #include "nsLayoutUtils.h"
@@ -2510,18 +2511,18 @@ nsresult HTMLMediaElement::InitializeDec
   if (!decoder->Init(this)) {
     LOG(PR_LOG_DEBUG, ("%p Failed to init cloned decoder %p", this, decoder.get()));
     return NS_ERROR_FAILURE;
   }
 
   double duration = aOriginal->GetDuration();
   if (duration >= 0) {
     decoder->SetDuration(duration);
-    decoder->SetTransportSeekable(aOriginal->IsTransportSeekable());
-    decoder->SetMediaSeekable(aOriginal->IsMediaSeekable());
+    decoder->GetStateMachine()->GetReader()->SetTransportSeekable(aOriginal->GetStateMachine()->GetReader()->CanSeekTransport());
+    decoder->GetStateMachine()->GetReader()->SetMediaSeekable(aOriginal->GetStateMachine()->GetReader()->CanSeekMedia());
   }
 
   nsRefPtr<MediaResource> resource = originalResource->CloneData(decoder);
   if (!resource) {
     LOG(PR_LOG_DEBUG, ("%p Failed to cloned stream for decoder %p", this, decoder.get()));
     return NS_ERROR_FAILURE;
   }
 
@@ -2837,17 +2838,18 @@ void HTMLMediaElement::MetadataLoaded(in
                                       bool aHasVideo,
                                       const MetadataTags* aTags)
 {
   mHasAudio = aHasAudio;
   mTags = aTags;
   ChangeReadyState(nsIDOMHTMLMediaElement::HAVE_METADATA);
   DispatchAsyncEvent(NS_LITERAL_STRING("durationchange"));
   DispatchAsyncEvent(NS_LITERAL_STRING("loadedmetadata"));
-  if (mDecoder && mDecoder->IsTransportSeekable() && mDecoder->IsMediaSeekable()) {
+  if (mDecoder && mDecoder->GetStateMachine()->GetReader()->CanSeekTransport() 
+      && mDecoder->GetStateMachine()->GetReader()->CanSeekMedia()) {
     ProcessMediaFragmentURI();
     mDecoder->SetFragmentEndTime(mFragmentEnd);
   }
 
   // If this element had a video track, but consists only of an audio track now,
   // delete the VideoFrameContainer. This happens when the src is changed to an
   // audio only file.
   if (!aHasVideo && mVideoFrameContainer) {
diff --git a/content/media/AbstractMediaDecoder.h b/content/media/AbstractMediaDecoder.h
--- a/content/media/AbstractMediaDecoder.h
+++ b/content/media/AbstractMediaDecoder.h
@@ -19,30 +19,37 @@ namespace layers
 {
   class ImageContainer;
 }
 class MediaResource;
 class ReentrantMonitor;
 class VideoFrameContainer;
 class TimedMetadata;
 class MediaDecoderOwner;
+class MediaDecoderStateMachine;
 
 typedef nsDataHashtable<nsCStringHashKey, nsCString> MetadataTags;
 
 static inline bool IsCurrentThread(nsIThread* aThread) {
   return NS_GetCurrentThread() == aThread;
 }
 
 /**
  * The AbstractMediaDecoder class describes the public interface for a media decoder
  * and is used by the MediaReader classes.
  */
 class AbstractMediaDecoder : public nsISupports
 {
 public:
+  // Returns the state machine if there is one (override by child class),
+  // else return null.
+  virtual MediaDecoderStateMachine* GetStateMachine() const {
+    return nullptr;
+  }
+
   // Returns the monitor for other threads to synchronise access to
   // state.
   virtual ReentrantMonitor& GetReentrantMonitor() = 0;
 
   // Returns true if the decoder is shut down.
   virtual bool IsShutdown() const = 0;
 
   virtual bool OnStateMachineThread() const = 0;
@@ -72,31 +79,19 @@ public:
   // Set the duration of the media in microseconds.
   virtual void SetMediaDuration(int64_t aDuration) = 0;
 
   // Sets the duration of the media in microseconds. The MediaDecoder
   // fires a durationchange event to its owner (e.g., an HTML audio
   // tag).
   virtual void UpdateEstimatedMediaDuration(int64_t aDuration) = 0;
 
-  // Set the media as being seekable or not.
-  virtual void SetMediaSeekable(bool aMediaSeekable) = 0;
-
-  // Set the transport level as being seekable or not.
-  virtual void SetTransportSeekable(bool aTransportSeekable) = 0;
-
   virtual VideoFrameContainer* GetVideoFrameContainer() = 0;
   virtual mozilla::layers::ImageContainer* GetImageContainer() = 0;
 
-  // Return true if the media layer supports seeking.
-  virtual bool IsTransportSeekable() = 0;
-
-  // Return true if the transport layer supports seeking.
-  virtual bool IsMediaSeekable() = 0;
-
   virtual void MetadataLoaded(int aChannels, int aRate, bool aHasAudio, bool aHasVideo, MetadataTags* aTags) = 0;
   virtual void QueueMetadata(int64_t aTime, int aChannels, int aRate, bool aHasAudio, bool aHasVideo, MetadataTags* aTags) = 0;
 
   // Set the media end time in microseconds
   virtual void SetMediaEndTime(int64_t aTime) = 0;
 
   // Make the decoder state machine update the playback position. Called by
   // the reader on the decoder thread (Assertions for this checked by
diff --git a/content/media/BufferDecoder.cpp b/content/media/BufferDecoder.cpp
--- a/content/media/BufferDecoder.cpp
+++ b/content/media/BufferDecoder.cpp
@@ -109,54 +109,30 @@ BufferDecoder::SetMediaDuration(int64_t 
 }
 
 void
 BufferDecoder::UpdateEstimatedMediaDuration(int64_t aDuration)
 {
   // ignore
 }
 
-void
-BufferDecoder::SetMediaSeekable(bool aMediaSeekable)
-{
-  // ignore
-}
-
-void
-BufferDecoder::SetTransportSeekable(bool aTransportSeekable)
-{
-  // ignore
-}
-
 VideoFrameContainer*
 BufferDecoder::GetVideoFrameContainer()
 {
   // no video frame
   return nullptr;
 }
 
 layers::ImageContainer*
 BufferDecoder::GetImageContainer()
 {
   // no image container
   return nullptr;
 }
 
-bool
-BufferDecoder::IsTransportSeekable()
-{
-  return false;
-}
-
-bool
-BufferDecoder::IsMediaSeekable()
-{
-  return false;
-}
-
 void
 BufferDecoder::MetadataLoaded(int aChannels, int aRate, bool aHasAudio, bool aHasVideo, MetadataTags* aTags)
 {
   // ignore
 }
 
 void
 BufferDecoder::QueueMetadata(int64_t aTime, int aChannels, int aRate, bool aHasAudio, bool aHasVideo, MetadataTags* aTags)
diff --git a/content/media/BufferDecoder.h b/content/media/BufferDecoder.h
--- a/content/media/BufferDecoder.h
+++ b/content/media/BufferDecoder.h
@@ -47,27 +47,19 @@ public:
   virtual int64_t GetEndMediaTime() const MOZ_OVERRIDE;
 
   virtual int64_t GetMediaDuration() MOZ_OVERRIDE;
 
   virtual void SetMediaDuration(int64_t aDuration) MOZ_OVERRIDE;
 
   virtual void UpdateEstimatedMediaDuration(int64_t aDuration) MOZ_OVERRIDE;
 
-  virtual void SetMediaSeekable(bool aMediaSeekable) MOZ_OVERRIDE;
-
-  virtual void SetTransportSeekable(bool aTransportSeekable) MOZ_OVERRIDE;
-
   virtual VideoFrameContainer* GetVideoFrameContainer() MOZ_OVERRIDE;
   virtual layers::ImageContainer* GetImageContainer() MOZ_OVERRIDE;
 
-  virtual bool IsTransportSeekable() MOZ_OVERRIDE;
-
-  virtual bool IsMediaSeekable() MOZ_OVERRIDE;
-
   virtual void MetadataLoaded(int aChannels, int aRate, bool aHasAudio, bool aHasVideo, MetadataTags* aTags) MOZ_OVERRIDE;
   virtual void QueueMetadata(int64_t aTime, int aChannels, int aRate, bool aHasAudio, bool aHasVideo, MetadataTags* aTags) MOZ_OVERRIDE;
 
   virtual void SetMediaEndTime(int64_t aTime) MOZ_OVERRIDE;
 
   virtual void UpdatePlaybackPosition(int64_t aTime) MOZ_OVERRIDE;
 
   virtual void OnReadMetadataCompleted() MOZ_OVERRIDE;
diff --git a/content/media/MediaDecoder.cpp b/content/media/MediaDecoder.cpp
--- a/content/media/MediaDecoder.cpp
+++ b/content/media/MediaDecoder.cpp
@@ -1274,59 +1274,26 @@ void MediaDecoder::UpdateEstimatedMediaD
 {
   if (mPlayState <= PLAY_STATE_LOADING) {
     return;
   }
   NS_ENSURE_TRUE_VOID(GetStateMachine());
   GetStateMachine()->UpdateEstimatedDuration(aDuration);
 }
 
-void MediaDecoder::SetMediaSeekable(bool aMediaSeekable) {
-  ReentrantMonitorAutoEnter mon(GetReentrantMonitor());
-  MOZ_ASSERT(NS_IsMainThread() || OnDecodeThread());
-  mMediaSeekable = aMediaSeekable;
-  if (mDecoderStateMachine) {
-    mDecoderStateMachine->SetMediaSeekable(aMediaSeekable);
-  }
-}
-
-void MediaDecoder::SetTransportSeekable(bool aTransportSeekable)
-{
-  ReentrantMonitorAutoEnter mon(GetReentrantMonitor());
-  MOZ_ASSERT(NS_IsMainThread() || OnDecodeThread());
-  mTransportSeekable = aTransportSeekable;
-  if (mDecoderStateMachine) {
-    mDecoderStateMachine->SetTransportSeekable(aTransportSeekable);
-  }
-}
-
-bool MediaDecoder::IsTransportSeekable()
-{
-  MOZ_ASSERT(NS_IsMainThread());
-  return mTransportSeekable;
-}
-
-bool MediaDecoder::IsMediaSeekable()
-{
-  NS_ENSURE_TRUE(GetStateMachine(), false);
-  ReentrantMonitorAutoEnter mon(GetReentrantMonitor());
-  MOZ_ASSERT(OnDecodeThread() || NS_IsMainThread());
-  return mMediaSeekable;
-}
-
 nsresult MediaDecoder::GetSeekable(dom::TimeRanges* aSeekable)
 {
   double initialTime = 0.0;
 
   // We can seek in buffered range if the media is seekable. Also, we can seek
   // in unbuffered ranges if the transport level is seekable (local file or the
   // server supports range requests, etc.)
-  if (!IsMediaSeekable()) {
+  if (!GetStateMachine()->GetReader()->CanSeekMedia()) {
     return NS_OK;
-  } else if (!IsTransportSeekable()) {
+  } else if (!GetStateMachine()->GetReader()->CanSeekTransport()) {
     return GetBuffered(aSeekable);
   } else {
     double end = IsInfinite() ? std::numeric_limits<double>::infinity()
                               : initialTime + GetDuration();
     aSeekable->Add(initialTime, end);
     return NS_OK;
   }
 }
diff --git a/content/media/MediaDecoder.h b/content/media/MediaDecoder.h
--- a/content/media/MediaDecoder.h
+++ b/content/media/MediaDecoder.h
@@ -595,26 +595,16 @@ public:
   // The duration is assumed to be an estimate, and so a degree of
   // instability is expected; if the incoming duration is not significantly
   // different from the existing duration, the change request is ignored.
   // If the incoming duration is significantly different, the duration is
   // changed, this causes a durationchanged event to fire to the media
   // element.
   void UpdateEstimatedMediaDuration(int64_t aDuration) MOZ_OVERRIDE;
 
-  // Set a flag indicating whether seeking is supported
-  virtual void SetMediaSeekable(bool aMediaSeekable) MOZ_OVERRIDE;
-  virtual void SetTransportSeekable(bool aTransportSeekable) MOZ_FINAL MOZ_OVERRIDE;
-  // Returns true if this media supports seeking. False for example for WebM
-  // files without an index and chained ogg files.
-  virtual bool IsMediaSeekable() MOZ_FINAL MOZ_OVERRIDE;
-  // Returns true if seeking is supported on a transport level (e.g. the server
-  // supports range requests, we are playing a file, etc.).
-  virtual bool IsTransportSeekable();
-
   // Return the time ranges that can be seeked into.
   virtual nsresult GetSeekable(dom::TimeRanges* aSeekable);
 
   // Set the end time of the media resource. When playback reaches
   // this point the media pauses. aTime is in seconds.
   virtual void SetFragmentEndTime(double aTime);
 
   // Set the end time of the media. aTime is in microseconds.
diff --git a/content/media/MediaDecoderReader.cpp b/content/media/MediaDecoderReader.cpp
--- a/content/media/MediaDecoderReader.cpp
+++ b/content/media/MediaDecoderReader.cpp
@@ -60,17 +60,19 @@ public:
   }
 
   size_t mSize;
 };
 
 MediaDecoderReader::MediaDecoderReader(AbstractMediaDecoder* aDecoder)
   : mAudioCompactor(mAudioQueue),
     mDecoder(aDecoder),
-    mIgnoreAudioOutputFormat(false)
+    mIgnoreAudioOutputFormat(false),
+    mMediaSeekable(true),
+    mTransportSeekable(true)
 {
   MOZ_COUNT_CTOR(MediaDecoderReader);
 }
 
 MediaDecoderReader::~MediaDecoderReader()
 {
   ResetDecode();
   MOZ_COUNT_DTOR(MediaDecoderReader);
@@ -331,16 +333,45 @@ nsresult MediaDecoderReader::DecodeToTar
   DECODER_LOG(PR_LOG_DEBUG,
               ("MediaDecoderReader::DecodeToTarget(%lld) finished v=%lld a=%lld",
               aTarget, v ? v->mTime : -1, a ? a->mTime : -1));
 #endif
 
   return NS_OK;
 }
 
+bool
+MediaDecoderReader::CanSeekMedia()
+{
+  NS_ENSURE_TRUE(mDecoder->GetStateMachine(), false);
+  MOZ_ASSERT(mDecoder->OnDecodeThread());
+  return mMediaSeekable;
+}
+
+bool
+MediaDecoderReader::CanSeekTransport()
+{
+  MOZ_ASSERT(mDecoder->OnDecodeThread());
+  return mTransportSeekable;
+}
+
+void
+MediaDecoderReader::SetMediaSeekable(bool aSeekable)
+{
+  MOZ_ASSERT(mDecoder->OnDecodeThread());
+  mMediaSeekable = aSeekable;
+}
+
+void
+MediaDecoderReader::SetTransportSeekable(bool aSeekable)
+{
+  MOZ_ASSERT(mDecoder->OnDecodeThread());
+  mTransportSeekable = aSeekable;
+}
+
 nsresult
 MediaDecoderReader::GetBuffered(mozilla::dom::TimeRanges* aBuffered,
                                 int64_t aStartTime)
 {
   MediaResource* stream = mDecoder->GetResource();
   int64_t durationUs = 0;
   {
     ReentrantMonitorAutoEnter mon(mDecoder->GetReentrantMonitor());
diff --git a/content/media/MediaDecoderReader.h b/content/media/MediaDecoderReader.h
--- a/content/media/MediaDecoderReader.h
+++ b/content/media/MediaDecoderReader.h
@@ -157,26 +157,38 @@ public:
   VideoData* DecodeToFirstVideoData();
 
   // Decodes samples until we reach frames required to play at time aTarget
   // (usecs). This also trims the samples to start exactly at aTarget,
   // by discarding audio samples and adjusting start times of video frames.
   nsresult DecodeToTarget(int64_t aTarget);
 
   MediaInfo GetMediaInfo() { return mInfo; }
+ 
+  // Indicates if the reader associated to the decoder is seekable,
+  // for the transport and the media
+  bool CanSeekMedia();
+  bool CanSeekTransport();
 
+  void SetMediaSeekable(bool aSeekable);
+  void SetTransportSeekable(bool aSeekable);
+  
 protected:
 
   // Reference to the owning decoder object.
   AbstractMediaDecoder* mDecoder;
 
   // Stores presentation info required for playback.
   MediaInfo mInfo;
 
   // Whether we should accept media that we know we can't play
   // directly, because they have a number of channel higher than
   // what we support.
   bool mIgnoreAudioOutputFormat;
+
+  // Indicates whether the media and transport are seekable.
+  bool mMediaSeekable;
+  bool mTransportSeekable;
 };
 
 } // namespace mozilla
 
 #endif
diff --git a/content/media/MediaDecoderStateMachine.cpp b/content/media/MediaDecoderStateMachine.cpp
--- a/content/media/MediaDecoderStateMachine.cpp
+++ b/content/media/MediaDecoderStateMachine.cpp
@@ -190,18 +190,16 @@ MediaDecoderStateMachine::MediaDecoderSt
   mPreservesPitch(true),
   mBasePosition(0),
   mAmpleVideoFrames(2),
   mLowAudioThresholdUsecs(LOW_AUDIO_USECS),
   mAmpleAudioThresholdUsecs(AMPLE_AUDIO_USECS),
   mDispatchedAudioDecodeTask(false),
   mDispatchedVideoDecodeTask(false),
   mAudioCaptured(false),
-  mTransportSeekable(true),
-  mMediaSeekable(true),
   mPositionChangeQueued(false),
   mAudioCompleted(false),
   mGotDurationFromMetaData(false),
   mDispatchedEventToDecode(false),
   mStopAudioThread(true),
   mQuickBuffering(false),
   mMinimizePreroll(false),
   mDecodeThreadWaiting(false),
@@ -209,16 +207,20 @@ MediaDecoderStateMachine::MediaDecoderSt
   mDispatchedDecodeMetadataTask(false),
   mDispatchedDecodeSeekTask(false),
   mLastFrameStatus(MediaDecoderOwner::NEXT_FRAME_UNINITIALIZED),
   mTimerId(0)
 {
   MOZ_COUNT_CTOR(MediaDecoderStateMachine);
   NS_ASSERTION(NS_IsMainThread(), "Should be on main thread.");
 
+  // Reinitialization of reader seekable values
+  mReader->SetTransportSeekable(true);
+  mReader->SetMediaSeekable(true);
+
   // Only enable realtime mode when "media.realtime_decoder.enabled" is true.
   if (Preferences::GetBool("media.realtime_decoder.enabled", false) == false)
     mRealTime = false;
 
   mAmpleVideoFrames =
     std::max<uint32_t>(Preferences::GetUint("media.video-queue.default-size", 10), 3);
 
   mBufferingWait = mRealTime ? 0 : BUFFERING_WAIT_S;
@@ -1247,25 +1249,25 @@ void MediaDecoderStateMachine::SetFragme
 }
 
 void MediaDecoderStateMachine::SetTransportSeekable(bool aTransportSeekable)
 {
   NS_ASSERTION(NS_IsMainThread() || OnDecodeThread(),
       "Should be on main thread or the decoder thread.");
   AssertCurrentThreadInMonitor();
 
-  mTransportSeekable = aTransportSeekable;
+  mReader->SetTransportSeekable(aTransportSeekable);
 }
 
 void MediaDecoderStateMachine::SetMediaSeekable(bool aMediaSeekable)
 {
   NS_ASSERTION(NS_IsMainThread() || OnDecodeThread(),
       "Should be on main thread or the decoder thread.");
 
-  mMediaSeekable = aMediaSeekable;
+  mReader->SetMediaSeekable(aMediaSeekable);
 }
 
 bool MediaDecoderStateMachine::IsDormantNeeded()
 {
   return mReader->IsDormantNeeded();
 }
 
 void MediaDecoderStateMachine::SetDormant(bool aDormant)
@@ -1415,17 +1417,17 @@ void MediaDecoderStateMachine::NotifyDat
 
 void MediaDecoderStateMachine::Seek(const SeekTarget& aTarget)
 {
   NS_ASSERTION(NS_IsMainThread(), "Should be on main thread.");
   ReentrantMonitorAutoEnter mon(mDecoder->GetReentrantMonitor());
 
   // We need to be able to seek both at a transport level and at a media level
   // to seek.
-  if (!mMediaSeekable) {
+  if (!mReader->CanSeekMedia()) {
     return;
   }
   // MediaDecoder::mPlayState should be SEEKING while we seek, and
   // in that case MediaDecoder shouldn't be calling us.
   NS_ASSERTION(mState != DECODER_STATE_SEEKING,
                "We shouldn't already be seeking");
   NS_ASSERTION(mState >= DECODER_STATE_DECODING,
                "We should have loaded metadata");
@@ -1826,23 +1828,23 @@ nsresult MediaDecoderStateMachine::Decod
   }
 
   if (mState == DECODER_STATE_SHUTDOWN) {
     return NS_ERROR_FAILURE;
   }
 
   NS_ASSERTION(mStartTime != -1, "Must have start time");
   MOZ_ASSERT((!HasVideo() && !HasAudio()) ||
-              !(mMediaSeekable && mTransportSeekable) || mEndTime != -1,
+             !(mReader->CanSeekMedia() && mReader->CanSeekTransport()) || mEndTime != -1,
               "Active seekable media should have end time");
-  MOZ_ASSERT(!(mMediaSeekable && mTransportSeekable) ||
+  MOZ_ASSERT(!(mReader->CanSeekMedia() && mReader->CanSeekTransport()) ||
              GetDuration() != -1, "Seekable media should have duration");
   DECODER_LOG(PR_LOG_DEBUG, "Media goes from %lld to %lld (duration %lld) "
               "transportSeekable=%d, mediaSeekable=%d",
-              mStartTime, mEndTime, GetDuration(), mTransportSeekable, mMediaSeekable);
+              mStartTime, mEndTime, GetDuration(), mReader->CanSeekMedia(), mReader->CanSeekTransport());
 
   if (HasAudio() && !HasVideo()) {
     // We're playing audio only. We don't need to worry about slow video
     // decodes causing audio underruns, so don't buffer so much audio in
     // order to reduce memory usage.
     mAmpleAudioThresholdUsecs /= NO_VIDEO_AMPLE_AUDIO_DIVISOR;
     mLowAudioThresholdUsecs /= NO_VIDEO_AMPLE_AUDIO_DIVISOR;
   }
diff --git a/content/media/MediaDecoderStateMachine.h b/content/media/MediaDecoderStateMachine.h
--- a/content/media/MediaDecoderStateMachine.h
+++ b/content/media/MediaDecoderStateMachine.h
@@ -135,16 +135,20 @@ public:
     DECODER_STATE_SHUTDOWN
   };
 
   State GetState() {
     AssertCurrentThreadInMonitor();
     return mState;
   }
 
+  MediaDecoderReader* GetReader() {
+    return mReader;
+  }
+
   // Set the audio volume. The decoder monitor must be obtained before
   // calling this.
   void SetVolume(double aVolume);
   void SetAudioCaptured(bool aCapture);
 
   // Check if the decoder needs to become dormant state.
   bool IsDormantNeeded();
   // Set/Unset dormant state.
@@ -278,26 +282,16 @@ public:
 
   void NotifyDataArrived(const char* aBuffer, uint32_t aLength, int64_t aOffset);
 
   int64_t GetEndMediaTime() const {
     AssertCurrentThreadInMonitor();
     return mEndTime;
   }
 
-  bool IsTransportSeekable() {
-    AssertCurrentThreadInMonitor();
-    return mTransportSeekable;
-  }
-
-  bool IsMediaSeekable() {
-    AssertCurrentThreadInMonitor();
-    return mMediaSeekable;
-  }
-
   // Returns the shared state machine thread.
   nsIEventTarget* GetStateMachineThread();
 
   // Calls ScheduleStateMachine() after taking the decoder lock. Also
   // notifies the decoder thread in case it's waiting on the decoder lock.
   void ScheduleStateMachineWithLockAndWakeDecoder();
 
   // Schedules the shared state machine thread to run the state machine
@@ -850,24 +844,16 @@ protected:
   // skipping up to the next keyframe.
   bool mSkipToNextKeyFrame;
 
   // True if we shouldn't play our audio (but still write it to any capturing
   // streams). When this is true, mStopAudioThread is always true and
   // the audio thread will never start again after it has stopped.
   bool mAudioCaptured;
 
-  // True if the media resource can be seeked on a transport level. Accessed
-  // from the state machine and main threads. Synchronised via decoder monitor.
-  bool mTransportSeekable;
-
-  // True if the media can be seeked. Accessed from the state machine and main
-  // threads. Synchronised via decoder monitor.
-  bool mMediaSeekable;
-
   // True if an event to notify about a change in the playback
   // position has been queued, but not yet run. It is set to false when
   // the event is run. This allows coalescing of these events as they can be
   // produced many times per second. Synchronised via decoder monitor.
   // Accessed on main and state machine threads.
   bool mPositionChangeQueued;
 
   // True if the audio playback thread has finished. It is finished
diff --git a/content/media/MediaResource.cpp b/content/media/MediaResource.cpp
--- a/content/media/MediaResource.cpp
+++ b/content/media/MediaResource.cpp
@@ -343,17 +343,17 @@ ChannelMediaResource::OnStartRequest(nsI
     if (seekable && boundedSeekLimit) {
       // If range requests are supported, and we did not see an unbounded
       // upper range limit, we assume the resource is bounded.
       dataIsBounded = true;
     }
 
     mDecoder->SetInfinite(!dataIsBounded);
   }
-  mDecoder->SetTransportSeekable(seekable);
+  mDecoder->GetStateMachine()->GetReader()->SetTransportSeekable(seekable);
   mCacheStream.SetTransportSeekable(seekable);
 
   {
     MutexAutoLock lock(mLock);
     mIsTransportSeekable = seekable;
     mChannelStatistics->Start();
   }
 
diff --git a/content/media/RtspMediaResource.cpp b/content/media/RtspMediaResource.cpp
--- a/content/media/RtspMediaResource.cpp
+++ b/content/media/RtspMediaResource.cpp
@@ -604,33 +604,33 @@ RtspMediaResource::OnConnected(uint8_t a
   }
 
   // If the duration is 0, imply the stream is live stream.
   if (duration) {
     // Not live stream.
     mRealTime = false;
     bool seekable = true;
     mDecoder->SetInfinite(false);
-    mDecoder->SetTransportSeekable(seekable);
+    mDecoder->GetStateMachine()->GetReader()->SetTransportSeekable(seekable);
     mDecoder->SetDuration(duration);
   } else {
     // Live stream.
     // Check the preference "media.realtime_decoder.enabled".
     if (!Preferences::GetBool("media.realtime_decoder.enabled", false)) {
       // Give up, report error to media element.
       nsCOMPtr<nsIRunnable> event =
         NS_NewRunnableMethod(mDecoder, &MediaDecoder::DecodeError);
       NS_DispatchToMainThread(event, NS_DISPATCH_NORMAL);
       return NS_ERROR_FAILURE;
     } else {
       mRealTime = true;
       bool seekable = false;
       mDecoder->SetInfinite(true);
-      mDecoder->SetTransportSeekable(seekable);
-      mDecoder->SetMediaSeekable(seekable);
+      mDecoder->GetStateMachine()->GetReader()->SetTransportSeekable(seekable);
+      mDecoder->GetStateMachine()->GetReader()->SetMediaSeekable(seekable);
     }
   }
   // Fires an initial progress event and sets up the stall counter so stall events
   // fire if no download occurs within the required time frame.
   mDecoder->Progress(false);
 
   MediaDecoderOwner* owner = mDecoder->GetMediaOwner();
   NS_ENSURE_TRUE(owner, NS_ERROR_FAILURE);
diff --git a/content/media/directshow/DirectShowReader.cpp b/content/media/directshow/DirectShowReader.cpp
--- a/content/media/directshow/DirectShowReader.cpp
+++ b/content/media/directshow/DirectShowReader.cpp
@@ -208,17 +208,17 @@ DirectShowReader::ReadMetadata(MediaInfo
   // Begin decoding!
   hr = mControl->Run();
   NS_ENSURE_TRUE(SUCCEEDED(hr), NS_ERROR_FAILURE);
 
   DWORD seekCaps = 0;
   hr = mMediaSeeking->GetCapabilities(&seekCaps);
   bool canSeek = ((AM_SEEKING_CanSeekAbsolute & seekCaps) == AM_SEEKING_CanSeekAbsolute);
   if (!canSeek) {
-    mDecoder->SetMediaSeekable(false);
+    mMediaSeekable = false;
   }
 
   int64_t duration = mMP3FrameParser.GetDuration();
   if (SUCCEEDED(hr)) {
     ReentrantMonitorAutoEnter mon(mDecoder->GetReentrantMonitor());
     mDecoder->SetMediaDuration(duration);
   }
 
diff --git a/content/media/fmp4/MP4Reader.cpp b/content/media/fmp4/MP4Reader.cpp
--- a/content/media/fmp4/MP4Reader.cpp
+++ b/content/media/fmp4/MP4Reader.cpp
@@ -241,17 +241,17 @@ MP4Reader::ReadMetadata(MediaInfo* aInfo
   Microseconds duration = mDemuxer->Duration();
   if (duration != -1) {
     ReentrantMonitorAutoEnter mon(mDecoder->GetReentrantMonitor());
     mDecoder->SetMediaDuration(duration);
   }
   // We can seek if we get a duration *and* the reader reports that it's
   // seekable.
   if (!mDecoder->GetResource()->IsTransportSeekable() || !mDemuxer->CanSeek()) {
-    mDecoder->SetMediaSeekable(false);
+    mMediaSeekable = false;
   }
 
   *aInfo = mInfo;
   *aTags = nullptr;
 
   return NS_OK;
 }
 
diff --git a/content/media/gstreamer/GStreamerReader.cpp b/content/media/gstreamer/GStreamerReader.cpp
--- a/content/media/gstreamer/GStreamerReader.cpp
+++ b/content/media/gstreamer/GStreamerReader.cpp
@@ -433,17 +433,17 @@ nsresult GStreamerReader::ReadMetadata(M
     if (gst_element_query_duration(GST_ELEMENT(mPlayBin),
       &format, &duration) && format == GST_FORMAT_TIME) {
 #endif
       ReentrantMonitorAutoEnter mon(mDecoder->GetReentrantMonitor());
       LOG(PR_LOG_DEBUG, "have duration %" GST_TIME_FORMAT, GST_TIME_ARGS(duration));
       duration = GST_TIME_AS_USECONDS (duration);
       mDecoder->SetMediaDuration(duration);
     } else {
-      mDecoder->SetMediaSeekable(false);
+      mMediaSeekable = false;
     }
   }
 
   int n_video = 0, n_audio = 0;
   g_object_get(mPlayBin, "n-video", &n_video, "n-audio", &n_audio, nullptr);
   mInfo.mVideo.mHasVideo = n_video != 0;
   mInfo.mAudio.mHasAudio = n_audio != 0;
 
diff --git a/content/media/mediasource/MediaSourceDecoder.cpp b/content/media/mediasource/MediaSourceDecoder.cpp
--- a/content/media/mediasource/MediaSourceDecoder.cpp
+++ b/content/media/mediasource/MediaSourceDecoder.cpp
@@ -417,18 +417,18 @@ MediaSourceReader::GetBuffered(dom::Time
   }
   aBuffered->Normalize();
   return NS_OK;
 }
 
 nsresult
 MediaSourceReader::ReadMetadata(MediaInfo* aInfo, MetadataTags** aTags)
 {
-  mDecoder->SetMediaSeekable(true);
-  mDecoder->SetTransportSeekable(false);
+  SetMediaSeekable(true);
+  SetTransportSeekable(false);
 
   MSE_DEBUG("%p: MSR::ReadMetadata pending=%u", this, mPendingDecoders.Length());
 
   InitializePendingDecoders();
 
   MSE_DEBUG("%p: MSR::ReadMetadata decoders=%u", this, mDecoders.Length());
 
   // XXX: Make subdecoder setup async, so that use cases like bug 989888 can
diff --git a/content/media/mediasource/SourceBuffer.cpp b/content/media/mediasource/SourceBuffer.cpp
--- a/content/media/mediasource/SourceBuffer.cpp
+++ b/content/media/mediasource/SourceBuffer.cpp
@@ -79,28 +79,16 @@ SubBufferDecoder::SetMediaDuration(int64
 }
 
 void
 SubBufferDecoder::UpdateEstimatedMediaDuration(int64_t aDuration)
 {
   //mParentDecoder->UpdateEstimatedMediaDuration(aDuration);
 }
 
-void
-SubBufferDecoder::SetMediaSeekable(bool aMediaSeekable)
-{
-  //mParentDecoder->SetMediaSeekable(aMediaSeekable);
-}
-
-void
-SubBufferDecoder::SetTransportSeekable(bool aTransportSeekable)
-{
-  //mParentDecoder->SetTransportSeekable(aTransportSeekable);
-}
-
 layers::ImageContainer*
 SubBufferDecoder::GetImageContainer()
 {
   return mParentDecoder->GetImageContainer();
 }
 
 MediaDecoderOwner*
 SubBufferDecoder::GetOwner()
diff --git a/content/media/mediasource/SubBufferDecoder.h b/content/media/mediasource/SubBufferDecoder.h
--- a/content/media/mediasource/SubBufferDecoder.h
+++ b/content/media/mediasource/SubBufferDecoder.h
@@ -38,18 +38,16 @@ public:
 
   virtual ReentrantMonitor& GetReentrantMonitor() MOZ_OVERRIDE;
   virtual bool OnStateMachineThread() const MOZ_OVERRIDE;
   virtual bool OnDecodeThread() const MOZ_OVERRIDE;
   virtual SourceBufferResource* GetResource() const MOZ_OVERRIDE;
   virtual void NotifyDecodedFrames(uint32_t aParsed, uint32_t aDecoded) MOZ_OVERRIDE;
   virtual void SetMediaDuration(int64_t aDuration) MOZ_OVERRIDE;
   virtual void UpdateEstimatedMediaDuration(int64_t aDuration) MOZ_OVERRIDE;
-  virtual void SetMediaSeekable(bool aMediaSeekable) MOZ_OVERRIDE;
-  virtual void SetTransportSeekable(bool aTransportSeekable) MOZ_OVERRIDE;
   virtual layers::ImageContainer* GetImageContainer() MOZ_OVERRIDE;
   virtual MediaDecoderOwner* GetOwner() MOZ_OVERRIDE;
 
   void NotifyDataArrived(const char* aBuffer, uint32_t aLength, int64_t aOffset)
   {
     mReader->NotifyDataArrived(aBuffer, aLength, aOffset);
 
     // XXX: Params make no sense to parent decoder as it relates to a
diff --git a/content/media/ogg/OggReader.cpp b/content/media/ogg/OggReader.cpp
--- a/content/media/ogg/OggReader.cpp
+++ b/content/media/ogg/OggReader.cpp
@@ -354,17 +354,17 @@ nsresult OggReader::ReadMetadata(MediaIn
 
   if (HasAudio() || HasVideo()) {
     ReentrantMonitorAutoEnter mon(mDecoder->GetReentrantMonitor());
 
     MediaResource* resource = mDecoder->GetResource();
     if (mDecoder->GetMediaDuration() == -1 &&
         !mDecoder->IsShutdown() &&
         resource->GetLength() >= 0 &&
-        mDecoder->IsMediaSeekable())
+        CanSeekMedia())
     {
       // We didn't get a duration from the index or a Content-Duration header.
       // Seek to the end of file to find the end time.
       mDecoder->GetResource()->StartSeekingForMetadata();
       int64_t length = resource->GetLength();
 
       NS_ASSERTION(length > 0, "Must have a content length to get end time");
 
@@ -378,17 +378,17 @@ nsresult OggReader::ReadMetadata(MediaIn
         LOG(PR_LOG_DEBUG, ("Got Ogg duration from seeking to end %lld", endTime));
       }
       mDecoder->GetResource()->EndSeekingForMetadata();
     } else if (mDecoder->GetMediaDuration() == -1) {
       // We don't have a duration, and we don't know enough about the resource
       // to try a seek. Abort trying to get a duration. This happens for example
       // when the server says it accepts range requests, but does not give us a
       // Content-Length.
-      mDecoder->SetTransportSeekable(false);
+      mTransportSeekable = false;
     }
   } else {
     return NS_ERROR_FAILURE;
   }
   *aInfo = mInfo;
 
   return NS_OK;
 }
@@ -608,17 +608,17 @@ bool OggReader::DecodeAudioData()
 
 void OggReader::SetChained(bool aIsChained) {
   {
     ReentrantMonitorAutoEnter mon(mMonitor);
     mIsChained = aIsChained;
   }
   {
     ReentrantMonitorAutoEnter mon(mDecoder->GetReentrantMonitor());
-    mDecoder->SetMediaSeekable(false);
+    mMediaSeekable = false;
   }
 }
 
 bool OggReader::ReadOggChain()
 {
   bool chained = false;
 #ifdef MOZ_OPUS
   OpusState* newOpusState = nullptr;
diff --git a/content/media/webm/WebMReader.cpp b/content/media/webm/WebMReader.cpp
--- a/content/media/webm/WebMReader.cpp
+++ b/content/media/webm/WebMReader.cpp
@@ -460,17 +460,17 @@ nsresult WebMReader::ReadMetadata(MediaI
       } else {
         Cleanup();
         return NS_ERROR_FAILURE;
       }
     }
   }
 
   // We can't seek in buffered regions if we have no cues.
-  mDecoder->SetMediaSeekable(nestegg_has_cues(mContext) == 1);
+  mMediaSeekable = (nestegg_has_cues(mContext) == 1);
 
   *aInfo = mInfo;
 
   *aTags = nullptr;
 
   return NS_OK;
 }
 
diff --git a/content/media/wmf/WMFReader.cpp b/content/media/wmf/WMFReader.cpp
--- a/content/media/wmf/WMFReader.cpp
+++ b/content/media/wmf/WMFReader.cpp
@@ -542,17 +542,17 @@ WMFReader::ReadMetadata(MediaInfo* aInfo
     mDecoder->SetMediaEndTime(duration);
   }
   // We can seek if we get a duration *and* the reader reports that it's
   // seekable.
   bool canSeek = false;
   if (FAILED(hr) ||
       FAILED(GetSourceReaderCanSeek(mSourceReader, canSeek)) ||
       !canSeek) {
-    mDecoder->SetMediaSeekable(false);
+    mMediaSeekable = false;
   }
 
   *aInfo = mInfo;
   *aTags = nullptr;
   // aTags can be retrieved using techniques like used here:
   // http://blogs.msdn.com/b/mf/archive/2010/01/12/mfmediapropdump.aspx
 
   return NS_OK;
